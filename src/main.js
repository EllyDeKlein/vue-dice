import Vue from 'vue'
import App from './App.vue'
import Dice from './components/Dice.vue'

Vue.component('Dice', Dice);

new Vue({
  el: '#app',
  render: h => h(App)
})
